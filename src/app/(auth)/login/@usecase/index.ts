'use client'

import { FormikHelpers, FormikProps, useFormik } from "formik"
import { useState } from "react"
import * as Yup from 'yup'
import { FormEvent } from "react";
import { useRouter } from "next/navigation";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";

interface IFormikValues {
  username: string;
  password: string;
}

interface IReturn {
  loading: boolean;
  formik: FormikProps<IFormikValues>
  router: AppRouterInstance
}

export const useLogin = (): IReturn =>{
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const loginSchema = Yup.object().shape({
    username: Yup.string()
      .required('Username is required'),
    password: Yup.string()
      .min(3, 'Minimum 3 symbols')
      .max(50, 'Maximum 50 symbols')
      .required('Password is required'),
  })

  const initialValues = {
    username: 'admin@demo.com',
    password: 'demo',
  }
  const formik = useFormik<IFormikValues>({
    initialValues,
    validationSchema: loginSchema,
    onSubmit: async (values, {setStatus, setSubmitting}) => {
      setLoading(true)
      try {
        // const {data: auth} = await login(values.email, values.password)
        // saveAuth(auth)
        // const {data: user} = await getUserByToken(auth.api_token)
        // setCurrentUser(user)
      } catch (error) {
        console.error(error)
        // saveAuth(undefined)
        // setStatus('The login details are incorrect')
        // setSubmitting(false)
        // setLoading(false)
      }
    },
  })
  return {
    loading,
    formik,
    router,
  }
}
