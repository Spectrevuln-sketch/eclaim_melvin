'use client'
import React from 'react'
import clsx from 'clsx'
import { useLogin } from './@usecase'
import { Box, Button, Grid, Input, Typography } from '@mui/material'
import { toAbsoluteUrl } from '@/utils'
import Image from 'next/image'
import CustomTextField from '@/components/inputs/CustomTextField'
import StrikeText from '@/components/striketext/StrikeText'

const Login = () => {
  const {formik, loading, router} = useLogin()
  return (
    <Box
    sx={{
      display: 'flex',
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: '100vh',
    }}
  >
    {/* left wrapper */}
    <Box
    sx={{
      display: 'flex',
      flex: 1,
      flexDirection: 'row'
    }}>
      <Box sx={{
        display: 'flex',
        flex: 1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingX: '6em',
        gap:'20px'
      }}>
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}>
        <Typography sx={{
          fontWeight: 700,
          fontSize: '20px'
        }}>Sign In</Typography>
        <Typography sx={{
          fontSize: '15px'
        }}>Your Social Campaigns</Typography>
        </Box>
        <Box sx={{
          display: 'flex',
          flexDirection: 'row',
          gap: 3
        }}>
          <Button variant='outlined' sx={{
            display: 'flex',
            flexDirection: 'row',
            gap: 2
          }}>
            <Image alt="login with apple" width={15} height={15} src={toAbsoluteUrl('/media/svg/brand-logos/apple-black.svg')}/>
            <Typography>
              Sign in With Apple
            </Typography>
          </Button>
          <Button variant='outlined' sx={{
            display: 'flex',
            flexDirection: 'row',
            gap: 2
          }}>
            <Image alt="google_icon" width={15} height={15} src={toAbsoluteUrl('/media/svg/brand-logos/google-icon.svg')}/>
            <Typography>
              Sign in With Google
            </Typography>
          </Button>
        </Box>
        <StrikeText text="Or with email"/>
        <CustomTextField
                label='username'
                isDisabled={false}
                isError={false}
                textHelper=''
                endAdornment=''
                // onChange={(val) => setPayload({
                //   ...payload,
                //   username:val
                // })}
              />
        <CustomTextField
                label='Password'
                placeholder=''
                isDisabled={false}
                isError={false}
                textHelper=''
                endAdornment=''
                // onChange={(val) => setPayload({
                //   ...payload,
                //   username:val
                // })}
              />
              <Typography>

              </Typography>
              <Button>Continue</Button>
      </Box>
    </Box>
    {/* right wrapper */}
    <Box sx={{
      display: 'flex',
      flex: 1,
      flexDirection: 'column'
    }}>
      <Box sx={{
        display: 'flex',
        flex: 1,
        flexDirection:'column',
        alignItems: 'center',
        backgroundColor: 'green'
      }}>

      <p>TEST1</p>
      </Box>
    </Box>
  </Box>
  )
}

export default Login