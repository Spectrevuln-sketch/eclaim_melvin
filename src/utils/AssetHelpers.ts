
export const toAbsoluteUrl = (pathname: string) =>
  process.env.NEXT_PUBLIC_URL_V1 + pathname;

