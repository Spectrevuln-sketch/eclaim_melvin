const colorStyle = {
  main: {
    lightest: '#6d8ec4',
    light: '#4971b5',
    main: '#365486',
    dark: '#2b446d',
    darkest: '#1d2d48',
    text: '#ddd',
    contrasText: '#fff',
  },
  waitingForApproval: {
    light: '#FCE4E4',
    main: '#EA5455'
  },
  approval: {
    light: '#DDF6E8',
    main: '#28C76F',
  },
  delivering: {
    light: '#EBFBFD',
    main: '#00CFE8'
  },
  delivered: {
    light: '#FBEFC8',
    main: '#F7C113'
  }
}

export default colorStyle;