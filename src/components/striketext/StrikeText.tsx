import { Box, Typography } from '@mui/material'
import React from 'react'
import { StrikeTextCSS } from './style'

interface IProps{
  text: string
}
const StrikeText: React.FC<IProps> = ({text}) => {
  return (
    <Box className="strikethrough-text">
    <StrikeTextCSS variant='body1'>{text}</StrikeTextCSS>
  </Box>
  )
}

export default StrikeText