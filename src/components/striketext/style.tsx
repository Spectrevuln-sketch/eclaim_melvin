import { Typography, styled } from "@mui/material";

export const Separator = styled.div`
  display: block;
  height: 0;
  border-bottom: 1px solid ${({ theme }) => theme.palette.borderColor};

  &.separator-dotted {
    border-bottom-style: dotted;
    border-bottom-color: ${({ theme }) => theme.palette.borderDashedColor};
  }

  &.separator-dashed {
    border-bottom-style: dashed;
    border-bottom-color: ${({ theme }) => theme.palette.borderDashedColor};
  }

  &.separator-content {
    display: flex;
    align-items: center;
    border-bottom: 0;
    text-align: center;

    &::before,
    &::after {
      content: " ";
      width: 50%;
      border-bottom: 1px solid ${({ theme }) => theme.palette.borderColor};
    }

    &::before {
      margin-right: 1.25rem;
    }

    &::after {
      margin-left: 1.25rem;
    }

    &.separator-dotted {
      &::before,
      &::after {
        border-bottom-style: dotted;
        border-bottom-color: ${({ theme }) => theme.palette.borderDashedColor};
      }
    }

    &.separator-dashed {
      &::before,
      &::after {
        border-bottom-style: dashed;
        border-bottom-color: ${({ theme }) => theme.palette.borderDashedColor};
      }
    }

    ${({ theme }) => {
      let styles = '';
      for (const color in theme.palette) {
        styles += `
          &.border-${color} {
            &::before,
            &::after {
              border-color: ${theme.palette[color]} !important;
            }
          }
        `;
      }
      return styles;
    }}
  }
`;