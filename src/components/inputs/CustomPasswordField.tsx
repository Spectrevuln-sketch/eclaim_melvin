'use client';

import { Visibility, VisibilityOff } from "@mui/icons-material";
import { FormControl, FormHelperText, IconButton, InputAdornment, OutlinedInput, Typography } from "@mui/material";
import React from "react";


type colorOptions = 'transparent' | 'white';
interface CustomTextFieldInterface {
  label: string;
  placeholder: string;
  isDisabled?: boolean;
  isError?: boolean;
  textHelper?: string;
  color?: colorOptions;
  name?: string;
  onChange: (val: string, e?: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
}

const CustomPasswordField: React.FC<CustomTextFieldInterface> = ({
  label,
  placeholder,
  textHelper = '',
  isDisabled = false,
  isError = false,
  color = 'transparent',
  name = 'password',
  onChange
}) => {

  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
  };

  return(
    <FormControl variant="outlined" fullWidth>
      {
        label !== ''
        ? <Typography
          id="custom-textfield-label"
          sx={{
            marginBottom: '4px',
            fontSize: '13px',
            color: color === 'white' ? '#fff' : '#4B465C'
          }}
        >
          {label}
        </Typography>
        : null
      }
      <OutlinedInput
        disabled={isDisabled}
        name={name}
        error={isError}
        fullWidth
        type={showPassword ? 'text' : 'password'}
        size="small"
        placeholder={placeholder}
        id="custom-textfield"
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              onMouseDown={handleMouseDownPassword}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        }
        aria-describedby="custom-text-field-helper-text"
        onChange={(e) => onChange(e.target.value, e)}
        inputProps={{
          'aria-label': 'textfield',
        }}
        sx={{
          borderRadius: '6px',
          color: '#A8AAAE',
          backgroundColor: color === 'white' ? '#fff' : undefined
        }}
      />
      <FormHelperText id="custom-text-field-helper-text">{textHelper}</FormHelperText>
    </FormControl>
  )
}

export default CustomPasswordField;